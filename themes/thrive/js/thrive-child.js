(function($){

	// console.log(this);

	$(window).load(function() {

		$('.flexslider').flexslider({

			animation: "slide",

			controlsContainer: $(".custom-controls-container"),

			customDirectionNav: $(".custom-navigation a")

		});

		$('#block-mobilemenutrigger').click(function(e){
			$('#block-thrive-main-menu ul').slideToggle('fast');
			$('#header, #block-thrive-main-menu ul').toggleClass('open');

		});

		// Set links active state
		$(function(){
			var current = location.pathname;
			$('.tabmenu li a').each(function(){
				var $this = $(this);
				var $parent = $(this).parent().parent().parent();
				// if the current path is like this link, make it active
				if($this.attr('href').indexOf(current) !== -1){
					$this.addClass('active');
					$parent.addClass('active');
				}
			})
		});

		// Smooth scroll when anchors are clicked
		$('#block-thrive-main-menu li a, #block-thrive-footer li a, #block-bookschedulemenu li:nth-child(1) a, .slides .btn.cta.15 a, .slides .btn.cta.17 a').click(function(){
			var anchor = "#"+$(this).attr('name');
			if ($('header').hasClass('scrolled')) {
				$('html, body').animate({
					scrollTop: $(anchor).offset().top-35
				}, 700);
			} else {
				$('html, body').animate({
					scrollTop: $(anchor).offset().top-135
				}, 700);
			}
			if ($('#block-thrive-main-menu ul').hasClass('open')) {
				$('#block-thrive-main-menu ul').slideToggle('fast').removeClass('open');
			}


			$('.anchor.active').removeClass('active');
			$(this).addClass('active');
			return false;
		});

		$('.resources .download .file a, #block-bookschedulemenu li:nth-child(2) a').attr('target', '_blank');



		// JS: Trigger popups
		$('div.team-bio-link').click(function(){

		  var anchor = $(this).attr('id');
		  var tid = anchor.substring(9);

		  var overlay_content = $('#team-bio-info-'+ tid + ' .overlay-content').html();
		  overlay_content = '<span class="close"></span>'+overlay_content;
		  show_overlay(overlay_content);
		  return false;
		});

		function show_overlay(overlay_content) {
		  $(document).find('.overlay').html("").html('<div class="overlay_content animated fadeIn br-x8 swing-in-top-fwd">'+overlay_content+'</div>').toggleClass('hidden');
		  $('body').addClass('noscroll');
		  centre_overlay();
		}

		function centre_overlay(){
		  h = $(window).height();
		  w = $(window).width();
		  if (w > 480) {
		    var overlay_ideal_height = h-250;
		    var overlay_max_height = overlay_ideal_height-250;
		  } else {
		    var overlay_ideal_height = h-180;
		    var overlay_max_height = overlay_ideal_height;
		  }

		  $(document).find('.overlay .overlay_content').css({'max-height':overlay_ideal_height, 'height':'auto'});

		  var overlayheader_height = $(document).find('.overlay .overlay_content .content_title').height();
		  var bodycontent_max_height =  overlay_ideal_height - overlayheader_height - 100;

		  $(document).find('.overlay .overlay_content .content_blurb').css({'max-height': bodycontent_max_height , 'height': 'auto'});

		  var margin_top = -(($(document).find('.overlay .overlay_content').height()/1.7));
		  $(document).find('.overlay .overlay_content').css('margin-top', margin_top);
		}


		// Closing overlays
		$(document).delegate('.close', 'click', function(e){
		  e.preventDefault();
		  $('body').removeClass('noscroll');
		  $(document).find('.overlay').addClass('hidden').html('');
		  $(document).find('.overlay *').remove();
		});

		$(document).keyup(function(e) {
		  if (e.keyCode == 27) $('.close').click();   // esc
		});

		function resize(w,h){
		  w = $(window).width();
		  centre_overlay();
		}

	});


	jQuery(document).ready(function($) {

		$('.tabmenu a').click(function(e) {
			$('.tabmenu a').removeClass('active');
			$(this).toggleClass('active');

			if($(this).hasClass( "learning" )){

				$('#Clinical.servicescontainer').hide();
				$('#Consultancy.servicescontainer').hide();
				$('#Learning.servicescontainer').hide();

				$('#Learning.servicescontainer').fadeIn( "slow");

			} else if ($(this).hasClass( "clinical" )){

				$('#Learning.servicescontainer').hide();
				$('#Consultancy.servicescontainer').hide();
				$('#Clinical.servicescontainer').hide();

				$('#Clinical.servicescontainer').fadeIn( "slow");

			} else if ($(this).hasClass( "consultancy" )){

				$('#Learning.servicescontainer').hide();
				$('#Clinical.servicescontainer').hide();
				$('#Consultancy.servicescontainer').hide();

				$('#Consultancy.servicescontainer').fadeIn( "slow");

			}

		});


		$('.clinic-locations-block .row').click(function(e) {
			// Close active
			if ($(document).find('#block-views-block-clinic-block-1 .field-content .active')) {
					$(document).find('#block-views-block-clinic-block-1 .field-content .active').removeClass('active').addClass('hidden');
			}
			$('.row.active').removeClass('active'); //remove any active class
			var country = $(this).attr('class');
			country = country.replace(/\s+/g, '.');
			var search_term = '#block-views-block-clinic-block-1 .'+country+' .hidden';
			$(document).find(search_term).removeClass('hidden').addClass('active');
			$(this).addClass('active');

			w = $(window).width();
			if (w <= 940) { // scroll down on mobile
				$('html, body').animate({
					scrollTop: $("#block-views-block-clinic-block-1").offset().top-35
				}, 700);
			}

		});

		// Clinic images for taxonomies
		$( ".clinic-locations-block .row" ).each(function() {
			var image_url = $(this).find('.field-content img').attr('src');
		 	$(this).css({
				'backgroundImage':'url('+image_url+')',
				'background-size':'cover',
			});
		});
		
		var configProfile = {
        	"profile": {"screenName": 'Thrive_WW'},
        	"domId": 'twitter-feed',
        	"maxTweets": 2,
        	"enableLinks": true,
        	"showInteraction": false,
        	"showUser": false,
        	"showTime": false,
        	"showImages": false,
        	"lang": 'en'
        };
        twitterFetcher.fetch(configProfile);

	});

	/********** COOKIE MESSAGE ************/

	if(jQuery.cookie('thriveconf') == null) {
	    jQuery('#block-cookiemsgblock').slideDown('slow');
	    jQuery.cookie('thriveconf', '1', {expires:90, path:'/'});
	}
	$('#close-cookie-msg').click(function(){
		jQuery('#block-cookiemsgblock').slideUp('slow');
	});


})(jQuery);

/*var configProfile = {
	"profile": {"screenName": 'Thrive_WW'},
	"domId": 'twitter-feed',
	"maxTweets": 2,
	"enableLinks": true,
	"showInteraction": false,
	"showUser": false,
	"showTime": false,
	"showImages": false,
	"lang": 'en'
};
twitterFetcher.fetch(configProfile);*/

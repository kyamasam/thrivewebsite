<?php

/* {# inline_template_start #}<div class="hidden">
<h3>{{ field_city_town }}</h3>
{{ field_image }}
<div class="details">
<span class="title">{{ title }}</span>
<span class="desc">{{ body }}</span>
<span class="cta">
  <a class="btn-green" href="mailto:info@thrive-worldwide.org">BOOK AN APPOINTMENT</a>
  {{ field_call_to_action }}
</span>
</div>
</div> */
class __TwigTemplate_ea413d29d50f667746fae74028c4b3ff3c0ae522b8b0be7ec0f21b9b7f64ab60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "{# inline_template_start #}<div class=\"hidden\">
<h3>{{ field_city_town }}</h3>
{{ field_image }}
<div class=\"details\">
<span class=\"title\">{{ title }}</span>
<span class=\"desc\">{{ body }}</span>
<span class=\"cta\">
  <a class=\"btn-green\" href=\"mailto:info@thrive-worldwide.org\">BOOK AN APPOINTMENT</a>
  {{ field_call_to_action }}
</span>
</div>
</div>"));

        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"hidden\">
<h3>";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_city_town"] ?? null), "html", null, true));
        echo "</h3>
";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image"] ?? null), "html", null, true));
        echo "
<div class=\"details\">
<span class=\"title\">";
        // line 5
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</span>
<span class=\"desc\">";
        // line 6
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["body"] ?? null), "html", null, true));
        echo "</span>
<span class=\"cta\">
  <a class=\"btn-green\" href=\"mailto:info@thrive-worldwide.org\">BOOK AN APPOINTMENT</a>
  ";
        // line 9
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_call_to_action"] ?? null), "html", null, true));
        echo "
</span>
</div>
</div>";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"hidden\">
<h3>{{ field_city_town }}</h3>
{{ field_image }}
<div class=\"details\">
<span class=\"title\">{{ title }}</span>
<span class=\"desc\">{{ body }}</span>
<span class=\"cta\">
  <a class=\"btn-green\" href=\"mailto:info@thrive-worldwide.org\">BOOK AN APPOINTMENT</a>
  {{ field_call_to_action }}
</span>
</div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 9,  84 => 6,  80 => 5,  75 => 3,  71 => 2,  68 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"hidden\">
<h3>{{ field_city_town }}</h3>
{{ field_image }}
<div class=\"details\">
<span class=\"title\">{{ title }}</span>
<span class=\"desc\">{{ body }}</span>
<span class=\"cta\">
  <a class=\"btn-green\" href=\"mailto:info@thrive-worldwide.org\">BOOK AN APPOINTMENT</a>
  {{ field_call_to_action }}
</span>
</div>
</div>", "");
    }
}

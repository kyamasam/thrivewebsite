<?php

/* {# inline_template_start #}<div class="servicescontainer" id="{{ title }}">
<!-- <h2><a href="{{ view_node }}">{{ title }}</a></h2>-->
<h2>{{ title }}</h2>
<div class="homeservicesintro"> {{ field_subtitle }} </div>
<div class="homeservicesbody"> {{ body_1 }} </div>
<div class="download">{{ view_node }}</div>
{{ edit_node }} 
</div> */
class __TwigTemplate_c1cd6ba60b1088ab22723fbbea6ea28b7b365fb3f626ba98301cfd03e44c95a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "{# inline_template_start #}<div class=\"servicescontainer\" id=\"{{ title }}\">
<!-- <h2><a href=\"{{ view_node }}\">{{ title }}</a></h2>-->
<h2>{{ title }}</h2>
<div class=\"homeservicesintro\"> {{ field_subtitle }} </div>
<div class=\"homeservicesbody\"> {{ body_1 }} </div>
<div class=\"download\">{{ view_node }}</div>
{{ edit_node }} 
</div>"));

        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"servicescontainer\" id=\"";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "\">
<!-- <h2><a href=\"";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["view_node"] ?? null), "html", null, true));
        echo "\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</a></h2>-->
<h2>";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</h2>
<div class=\"homeservicesintro\"> ";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_subtitle"] ?? null), "html", null, true));
        echo " </div>
<div class=\"homeservicesbody\"> ";
        // line 5
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["body_1"] ?? null), "html", null, true));
        echo " </div>
<div class=\"download\">";
        // line 6
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["view_node"] ?? null), "html", null, true));
        echo "</div>
";
        // line 7
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edit_node"] ?? null), "html", null, true));
        echo " 
</div>";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"servicescontainer\" id=\"{{ title }}\">
<!-- <h2><a href=\"{{ view_node }}\">{{ title }}</a></h2>-->
<h2>{{ title }}</h2>
<div class=\"homeservicesintro\"> {{ field_subtitle }} </div>
<div class=\"homeservicesbody\"> {{ body_1 }} </div>
<div class=\"download\">{{ view_node }}</div>
{{ edit_node }} 
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 7,  83 => 6,  79 => 5,  75 => 4,  71 => 3,  65 => 2,  60 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"servicescontainer\" id=\"{{ title }}\">
<!-- <h2><a href=\"{{ view_node }}\">{{ title }}</a></h2>-->
<h2>{{ title }}</h2>
<div class=\"homeservicesintro\"> {{ field_subtitle }} </div>
<div class=\"homeservicesbody\"> {{ body_1 }} </div>
<div class=\"download\">{{ view_node }}</div>
{{ edit_node }} 
</div>", "");
    }
}

<?php

/* {# inline_template_start #}<div class="image">{{ field_image }}</div>
<div class="text">
{{ nothing }}
<div class="title">{{ title }}</div> 
<div class="blurb">{{ body }}</div>
<div class="link-bt"><a href="mailto: info@thrive-worldwide.org">Book Now</a></div>
<!--<div class="link">{{ view_node }}</div>-->
{{ edit_node }} 
</div> */
class __TwigTemplate_27574efaf0288d6fd4d20282e549f7ae25e5d4b8756ef9d9516c4199bd499f3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "{# inline_template_start #}<div class=\"image\">{{ field_image }}</div>
<div class=\"text\">
{{ nothing }}
<div class=\"title\">{{ title }}</div> 
<div class=\"blurb\">{{ body }}</div>
<div class=\"link-bt\"><a href=\"mailto: info@thrive-worldwide.org\">Book Now</a></div>
<!--<div class=\"link\">{{ view_node }}</div>-->
{{ edit_node }} 
</div>"));

        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"image\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image"] ?? null), "html", null, true));
        echo "</div>
<div class=\"text\">
";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nothing"] ?? null), "html", null, true));
        echo "
<div class=\"title\">";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</div> 
<div class=\"blurb\">";
        // line 5
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["body"] ?? null), "html", null, true));
        echo "</div>
<div class=\"link-bt\"><a href=\"mailto: info@thrive-worldwide.org\">Book Now</a></div>
<!--<div class=\"link\">";
        // line 7
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["view_node"] ?? null), "html", null, true));
        echo "</div>-->
";
        // line 8
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edit_node"] ?? null), "html", null, true));
        echo " 
</div>";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"image\">{{ field_image }}</div>
<div class=\"text\">
{{ nothing }}
<div class=\"title\">{{ title }}</div> 
<div class=\"blurb\">{{ body }}</div>
<div class=\"link-bt\"><a href=\"mailto: info@thrive-worldwide.org\">Book Now</a></div>
<!--<div class=\"link\">{{ view_node }}</div>-->
{{ edit_node }} 
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 8,  81 => 7,  76 => 5,  72 => 4,  68 => 3,  62 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"image\">{{ field_image }}</div>
<div class=\"text\">
{{ nothing }}
<div class=\"title\">{{ title }}</div> 
<div class=\"blurb\">{{ body }}</div>
<div class=\"link-bt\"><a href=\"mailto: info@thrive-worldwide.org\">Book Now</a></div>
<!--<div class=\"link\">{{ view_node }}</div>-->
{{ edit_node }} 
</div>", "");
    }
}

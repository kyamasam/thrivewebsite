<?php

/* {# inline_template_start #}<div id="team-bio-{{ nid }}" class="team-bio-link popup-trigger">Read bio <span class="overlay-icon"></span></div>
<div id="team-bio-info-{{ nid }}" class="hidden full-bio"><div class="overlay-content"><div class="content_blurb"><div class="content_image">{{ field_image }}</div><div class="content_title">{{ title }}</div><div class="content_sub_title">{{ field_subtitle }} </div><div class="content_body">{{ body }}</div></div></div></div> */
class __TwigTemplate_fa0e30161c139af7f88d28eaeea671b630987a46f599aaf3c616f2e6289adeef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "{# inline_template_start #}<div id=\"team-bio-{{ nid }}\" class=\"team-bio-link popup-trigger\">Read bio <span class=\"overlay-icon\"></span></div>
<div id=\"team-bio-info-{{ nid }}\" class=\"hidden full-bio\"><div class=\"overlay-content\"><div class=\"content_blurb\"><div class=\"content_image\">{{ field_image }}</div><div class=\"content_title\">{{ title }}</div><div class=\"content_sub_title\">{{ field_subtitle }} </div><div class=\"content_body\">{{ body }}</div></div></div></div>"));

        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div id=\"team-bio-";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nid"] ?? null), "html", null, true));
        echo "\" class=\"team-bio-link popup-trigger\">Read bio <span class=\"overlay-icon\"></span></div>
<div id=\"team-bio-info-";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nid"] ?? null), "html", null, true));
        echo "\" class=\"hidden full-bio\"><div class=\"overlay-content\"><div class=\"content_blurb\"><div class=\"content_image\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image"] ?? null), "html", null, true));
        echo "</div><div class=\"content_title\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</div><div class=\"content_sub_title\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_subtitle"] ?? null), "html", null, true));
        echo " </div><div class=\"content_body\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["body"] ?? null), "html", null, true));
        echo "</div></div></div></div>";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div id=\"team-bio-{{ nid }}\" class=\"team-bio-link popup-trigger\">Read bio <span class=\"overlay-icon\"></span></div>
<div id=\"team-bio-info-{{ nid }}\" class=\"hidden full-bio\"><div class=\"overlay-content\"><div class=\"content_blurb\"><div class=\"content_image\">{{ field_image }}</div><div class=\"content_title\">{{ title }}</div><div class=\"content_sub_title\">{{ field_subtitle }} </div><div class=\"content_body\">{{ body }}</div></div></div></div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 2,  48 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div id=\"team-bio-{{ nid }}\" class=\"team-bio-link popup-trigger\">Read bio <span class=\"overlay-icon\"></span></div>
<div id=\"team-bio-info-{{ nid }}\" class=\"hidden full-bio\"><div class=\"overlay-content\"><div class=\"content_blurb\"><div class=\"content_image\">{{ field_image }}</div><div class=\"content_title\">{{ title }}</div><div class=\"content_sub_title\">{{ field_subtitle }} </div><div class=\"content_body\">{{ body }}</div></div></div></div>", "");
    }
}
